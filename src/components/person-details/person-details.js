import React, {Component} from "react";
import './person-details.css';
import SwapiService from "../../services/swapi-service";
import Spinner from "../spinner";
import ErrorButton from "../error-button";

export default class PersonDetails extends Component{

    swapiService = new SwapiService();

    state = {
        person: null,
        loading: false
    };

    componentDidMount() {
        this.updatePerson();
    }

    componentDidUpdate(prevProps) {
        if (this.props.personId !== prevProps.personId) {
            this.setState({loading: true});
            this.updatePerson();
        }
    }

    updatePerson() {
        const { personId } = this.props;
            if (!personId) {
                return;
            }

            this.swapiService.getPerson(personId)
                .then((person) => {
                    this.setState({person, loading: false});
                })
    }

    render() {
        const { loading } = this.state;

        if (!this.state.person){
            return (
                <div className="person-details">
                Choose a person from the list
                </div>
            )
        }
        const spinner = loading ? <Spinner/> :  null;

        // const personDetailsView = !loading ? <PersonDetailsView person={this.state.person}/> : null;
        const personDetailsView = <PersonDetailsView person={this.state.person}/>;

        return(
          <div className="container person-details__container">
              {spinner}
              {personDetailsView}

          </div>
      );
    };
};
const PersonDetailsView = ({person}) => {
    const {id, name, gender, birthYear, eyeColor } = person;
    return(
        <div className="row person-details">
            <div className="col-sm-4 person-details__img">
                <img src={`http://starwars-visualguide.com/assets/img/characters/${id}.jpg`} alt=""/>
            </div>
            <div className="col-sm-8 person-details__info">
                <h3>{name}</h3>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                        <span className="term">Gender :</span>
                        <span> {gender}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">birthYear : </span>
                        <span> {birthYear}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">eyeColor : </span>
                        <span> {eyeColor}</span>
                    </li>
                    <ErrorButton/>
                </ul>
            </div>
        </div>
    );
};