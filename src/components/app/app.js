import React, {Component} from "react";
import './app.css';
import Header from '../header';
import RandomPlanet from '../random-planet';
// import Itemlist from "../item-list";
// import PersonDetails from '../person-details';
import PeoplePage from "../people-page/people-page";
import Itemlist from "../item-list";
import PersonDetails from "../person-details";
import SwapiService from "../../services/swapi-service";
// import PlanetDetails from '../planet-details';



export default class App extends Component {

    swapiService = new SwapiService();

    state = {

    };


    render() {
        return(
            <div>
                <Header />
                <RandomPlanet/>
                <PeoplePage/>
                <div className="container container__main">
                    <div className="row">
                        <Itemlist
                        OnItemSelected={this.onPersonSelected}
                        getData = {this.swapiService.getAllPlanets}
                        renderItem={(item) => item.name}/>
                        <PersonDetails personId={this.state.selectedPerson} />
                    </div>
                </div>
                <div className="container container__main">
                    <div className="row">
                        <Itemlist
                        OnItemSelected={this.onPersonSelected}
                        getData = {this.swapiService.getAllStarships}
                        renderItem={(item) => item.name}/>
                        <PersonDetails personId={this.state.selectedPerson} />
                    </div>
                </div>
            </div>
        );
    };
};


