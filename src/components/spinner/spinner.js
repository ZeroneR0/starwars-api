import React from "react";
import "./spinner.css"

const Spinner = () => {
    return (
        <div className="loadingio-spinner-double-ring-syiivo0a54">
            <div className="ldio-7rfqzo2nvoe">
                <div></div>
                <div></div>
                <div>
                    <div></div>
                </div>
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    )
};

export default Spinner;