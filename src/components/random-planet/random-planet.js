import React, {Component} from "react";
import './random-planet.css';
import SwapiService from "../../services/swapi-service";
import Spinner from "../spinner";
import ErrorIndicator from "../error-indicator";
import ErrorButton from "../error-button";


export default class RandomPlanet extends Component{

    swapiService = new SwapiService();

    state = {
      planet: {},
      loading: true,
      error: false
    };


    componentDidMount() {
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, 10000);

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }



    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loading: false,
            error: false
        });
    };

    onError = (err) => {
        this.setState({
            error: true,
            loading: false
        });
    };


    updatePlanet = () => {
        const id = Math.floor(Math.random()*25) + 3;
        this.swapiService
            .getPlanet(id)
            .then(this.onPlanetLoaded)
            .catch(this.onError);
    };


    render() {

        const { planet, loading, error} = this.state;
        const hasDate = !(loading || error);
        const errorMessage = error ? <ErrorIndicator/> : null;
        const spinner = loading ? <Spinner/> : null;
        const content = hasDate ? <PlanetView planet={planet}/> : null;


        return(
            <div className="container">
                <div className="row random-planet">
                    {errorMessage}
                    {spinner}
                    {content}
                </div>
                <ErrorButton/>
            </div>
       );

    };
};

const PlanetView = ({ planet }) => {

    const {name, population, rotationPeriod, diameter, id} = planet;

    return(
        <React.Fragment>
                <div className="col-sm-4 random-planet__img">
                    <img src={`http://starwars-visualguide.com/assets/img/planets/${id}.jpg`} alt=""/>
                </div>
                <div className="col-sm-8 random-planet__info">
                    <h1>{name}</h1>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                            <span>Population</span>
                            <span>{population}</span>
                        </li>
                        <li className="list-group-item">
                            <span>Rotation Period</span>
                            <span>{rotationPeriod}</span>
                        </li>
                        <li className="list-group-item">
                            <span>Diameter</span>
                            <span>{diameter}</span>
                        </li>
                    </ul>
            </div>
        </React.Fragment>
    );
};