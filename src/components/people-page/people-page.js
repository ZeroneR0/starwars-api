import React, {Component} from "react";

import './people-page.css'
import Itemlist from "../item-list";
import PersonDetails from "../person-details";
import ErrorIndicator from "../error-indicator";
import SwapiService from "../../services/swapi-service";


const Row = ({left, right}) => {
    return (
        <div className="container container__main">
            <div className="row">
                <div className="col-sm-6">
                    {left}
                </div>
                <div className="col-sm-6">
                    {right}
                </div>
            </div>
        </div>
    );
};


export default class PeoplePage extends Component{

    swapiService = new SwapiService();

    state = {
        selectedPerson: 3,
        hasError: false
    };

    componentDidCatch(error) {
        this.setState({
            hasError: true
        });
    };

    onPersonSelected = (id) => {
        this.setState({
            selectedPerson: id
        });

    };
    render() {

        if( this.state.hasError) {
            return <ErrorIndicator/>
        };

        const itemList = (
            <Itemlist OnItemSelected={this.onPersonSelected}
                      getData = {this.swapiService.getAllPeople}
                      renderItem={({name, gender, birthYear}) => `${name} (${gender} , ${birthYear})`}/>
        );

        const personDetails = (
            <PersonDetails personId={this.state.selectedPerson} />
        );

        return (
            <Row left = {itemList} right ={personDetails}/>
        );
    };
};