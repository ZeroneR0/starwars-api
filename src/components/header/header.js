import React from 'react'
import './header.css'

const Header = () =>{
  return(
      <div className="container header-wrapper">
        <div className="row">
            <div className="col-sm-3 header-title">
            STAR DB
            </div>
            <ul className="col-sm-9 nav header-nav">
                <li className="nav-item ">
                    <a className="nav-link" href="#">People</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">Planets</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">Starships</a>
                </li>
            </ul>
        </div>
      </div>

  )
};


export default Header;